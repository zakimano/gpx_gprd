#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define RAD 6371000 //Earth's mean radius in meters

#define HINC 1000 //Heap incremental, !!! ALSO USED FOR OTHER BUFFERS !!!
// KEEP HINC BETWEEN 1000 AND 10 000 !!

#define HBUF 10 //Heap buffer before increasing - 10 seems to work just fine.

#define ERROR_HANDLE(err) ({\
if (err != 0) {\
	if (evals != NULL)\
		free(evals);\
	if (firstInWpt != NULL)\
		delete_waypt(firstInWpt);\
	if (firstInWpt2 != NULL)\
		delete_waypt(firstInWpt2);\
	if (firstInList != NULL)\
		delete_track(firstInList);\
	if (firstInList2 != NULL)\
		delete_track(firstInList2);\
	if (strInput != NULL)\
		free(strInput);\
	if (strInput2 != NULL)\
		free(strInput2);\
return err;}\
})

#define SEH(err) ({if (err != 0) {return err;}})

/*
 * Above macro checks for allocated memory on given (static) adresses, and frees them, in case of an error.
*/

/* TO DO */
/*
 * gpx output: switch for one line / indent
 * metadata & waypoint handling
 * plot: counting with small & big - scaling
 * plot: analyze print
 * plot: heightmap
 * fastest: algorithm change? //dropped due extreme complexity of implementation, would be faster though.
 * area
 * intersect
 * 
 */

/* includes of header files in 'headers/' */
/* These are basic functions 'parsed' into this file in compile time */
/* Easier to extend/modify stuff like this - but watch out for inclusion order! */

#include "headers/handle_track.h"

#include "headers/handle_waypt.h"

#include "headers/handle_coord.h"

#include "headers/convert.h"

#include "headers/parse.h"

#include "headers/calc_sh.h"

#include "headers/haversine.h"

#include "headers/read_file.h"

#include "headers/read_source.h"

#include "headers/analyze.h"

#include "headers/compress.h"

#include "headers/print_gpx.h"

#include "headers/plotv2.h" //testing

#include "headers/fastest.h"

#include "headers/correct.h"

#include "headers/intersect.h" //wip

#include "headers/area.h" //confirmed working, it has some problems.


/* In case of extensions, include them above. */

#include "headers/arg-input_eval.h"

/* ONLY MAIN BELOW THIS POINT */
/* main() */

int main(int argc, char **argv)
{
	clock_t begin = clock();
	
	int err = 0;
	FILE *inputfile = NULL;
	FILE *inputfile2 = NULL;
	FILE *outputfile = NULL;
	char *evals = NULL;
	char *strInput = NULL;
	char *strInput2 = NULL;
	track* firstInList = NULL;
	waypt* firstInWpt = NULL;
	track* firstInList2 = NULL;
	waypt* firstInWpt2 = NULL;
	
	/* WATCH OUT FOR INPUT */
	
	err = inputeval(argc, argv, &inputfile, &inputfile2, &outputfile, &evals);
	ERROR_HANDLE(err);
	
	/* FOPEN */
	
	printf("Reading...\n");
	
	err = readFile(inputfile, &strInput, HINC);
	ERROR_HANDLE(err);
	
	if (inputfile2 != NULL)
	{
		err = readFile(inputfile2, &strInput2, HINC);
		ERROR_HANDLE(err);
	}
	
	/* FCLOSE */
	
	fclose(inputfile);
	
	if (inputfile2 != NULL)
	{
		fclose(inputfile2);
	}
	
	printf("Done.\n");
	
	/* READING DONE, TIME TO PARSE */
	
	printf("Processing...\n");
	
	err = parse(strInput, &firstInList, &firstInWpt);
	ERROR_HANDLE(err);
	
	if (strInput2 != NULL)
	{
		err = parse(strInput2, &firstInList2, &firstInWpt2);
		ERROR_HANDLE(err);
	}
	
	printf("Done.\n");
	
	/* PREPARATIONS DONE, TIME TO DO STUFF */
	
	if (strcmp(argv[1], "analyze") == 0)
	{
		printf("Analizing...\n");
		int fullTime;
		unsigned int pointCount;
		time_t beginTime;
		double fullDist, avgSpeed, maxSpeed, TdDist, avgTDspeed, totalUp, totalDown;
		err = analyze(firstInList, &fullDist, &TdDist, &avgSpeed, &maxSpeed, &avgTDspeed, &totalUp, &totalDown, &fullTime, &beginTime, &pointCount);
		ERROR_HANDLE(err);
		
		printf("Done.\n");
		p_analyze(fullDist, avgSpeed, maxSpeed, TdDist, avgTDspeed, totalUp, totalDown, fullTime, beginTime, pointCount);
	}
	
	if (strcmp(argv[1], "compress") == 0)
	{
		printf("Compressing...\n");
		double arg_angle;
		arg_angle = atof(argv[3]);
		err = compress(firstInList, &arg_angle);
		ERROR_HANDLE(err);
		
		printf("Done.\nPrinting output...\n");
		err = printOutputGPX(outputfile, firstInList, NULL);
		ERROR_HANDLE(err);
		
		printf("Done.\n");
		fclose(outputfile);
	}
	
	if (strcmp(argv[1], "plot") == 0)
	{
		printf("Plotting...\n");
		err = plot(outputfile, firstInList, firstInWpt, atoi(argv[4]), atoi(argv[5]));
		ERROR_HANDLE(err);
		
		printf("Done.\n");
		fclose(outputfile);
	}
	
	if (strcmp(argv[1], "correct") == 0)
	{
		printf("Correcting...\n");
		double arg_speed;
		arg_speed = (atof(argv[4]) / 3.6);
		err = correct(firstInList, &arg_speed);
		ERROR_HANDLE(err);
		
		printf("Done.\nPrinting output...\n");
		err = printOutputGPX(outputfile, firstInList, NULL);
		ERROR_HANDLE(err);
		
		printf("Done.\n");
		fclose(outputfile);
	}
	
	if (strcmp(argv[1], "fastest") == 0)
	{
		printf("Calculating fastest...\n");
		double arg_dist = atof(argv[3]);
		double fastSpeed, fastDist, fastBegin;
		int fastTime;
		err = fastest(firstInList, &fastBegin, &fastSpeed, &fastDist, &fastTime, &arg_dist);
		ERROR_HANDLE(err);
		
		printf("Done.\n");
		p_fastest(fastSpeed, fastDist, fastBegin, fastTime, arg_dist);
	}
	
	if (strcmp(argv[1], "area") == 0)
	{
		printf("Calculating area...\n");
		
		err = area(firstInList);
		ERROR_HANDLE(err);
		
		printf("Done.\n");
	}
	
	if (strcmp(argv[1], "intersect") == 0)
	{
		printf("Calculating intersections...\n");
		waypt *firstInInter = NULL;
		int found = 0;
		
		if (firstInList2 == NULL)
		{
			printf("Calculating self-intersections of the track...\n");
			selfIntersect(firstInList, &firstInInter, &found);
			printOutputGPX(outputfile, firstInList, firstInInter);
			printf("%d points found.\n", found);
		}
		else
		{
			printf("Calculating intersections of the two tracks...\n");
			ABtrackIntersect(firstInList, firstInList2, &firstInInter, &found);
			printOutputGPX(outputfile, firstInList, firstInInter);
			printf("%d points found.\n", found);
		}
		
		if (firstInInter != NULL)
		{
			free(firstInInter);
		}
		
		printf("Done.\n");
	}
	
	/* FREE STUFF */
	
	
	if (evals != NULL)
		free(evals);
	if (firstInWpt != NULL)
		delete_waypt(firstInWpt);
	if (firstInWpt2 != NULL)
		delete_waypt(firstInWpt2);
	if (firstInList != NULL)
		delete_track(firstInList);
	if (firstInList2 != NULL)
		delete_track(firstInList2);
	if (strInput != NULL)
		free(strInput);
	if (strInput2 != NULL)
		free(strInput2);
	
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Time spent: %f seconds\n", time_spent);
	
	/* RETURN
	 * 0: Normal
	 * 1: Memory error
	 * 2: Bad input
	 * 3: Missing files or other error
	 * 4: Help
	 */
	
	return 0;
}

/*
 * COMMENTS SECTION
 */

/*
 analyze trackfile
 Kimenet:
 Teljes hossz:           %.3f km
 Teljes 3D hossz:        %.3f km
 Teljes ido:             %2d ora %02d perc %02d masodperc
 Atlagsebesseg:          %.2f km/h
 Maximum sebesseg:       %.2f km/h
 Teljes szint fel:       %d m
 Teljes szint le:        %d m
 
 compress inputfile angle outputfile
 angle: fokban, 0-tol 180-ig lehet
 
 plot inputfile svgfile width height
 
 correct inputfile treshold outputfile
 treshold: km/h-ban ertve
 
 fastest inputfile distance
 distance: meterben megadva
 Kimenet:
 Leggyorsabb %d meter %.3f km-tol %.3f km-ig.
 Tenyleges hossz:        %d meter
 Ido:                    %2d ora %02d perc %02d masodperc
 Atlagsebesseg:          %.2f km/h
 
 
 Ha meg valami ezek utan sem kelloen specifikalt, kerlek irjatok meg.
 
 Egy jotanacs:
 
 Teszteljetek le a kodotokat, mielott elkulditek.
 Futtassatok a correctet tul nagy tresholddal, akkor elmeletileg semmit nem szabadna kiszornia.
 Ha megis, valami bugos. Ugyanigy ha valami nagyon picivel, kb mindent ki kell szornia.
 Ha valami normalis koztes ertekkel nezitek, akkor plotolas utan latszodik, hogy ilyen "levagottak" lesznek a kanyarok, ha jo.
 Ezek utan ha megy az analyze, akkor nem lehet a max sebesseg a megadott treshold felett ugye.
 (de sok peldat lattunk ra, hogy volt.)
 */
