#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'plot'
 */

void latlonminmax(track *trackToMeasure, double *latmin, double *latmax, double *lonmin, double *lonmax, double *ele_min, double *ele_max, unsigned int *all)
{
	track *iter;
	double t_latmin, t_latmax, t_lonmin, t_lonmax, t_ele_min, t_ele_max;
	unsigned int a = 0;
	
	iter = trackToMeasure;
	
	while (iter != NULL)
	{
		if (iter == trackToMeasure)
		{
			t_latmax = t_latmin = iter->lat;
			t_lonmax = t_lonmin = iter->lon;
			t_ele_min = t_ele_max = iter->ele;
		}
		else
		{
			if (t_latmax < iter->lat)
			{
				t_latmax = iter->lat;
			}
			if (t_latmin > iter->lat)
			{
				t_latmin = iter->lat;
			}
			if (t_lonmax < iter->lon)
			{
				t_lonmax = iter->lon;
			}
			if (t_lonmin > iter->lon)
			{
				t_lonmin = iter->lon;
			}
			if (t_ele_min > iter->ele)
			{
				t_ele_min = iter->ele;
			}
			if (t_ele_max < iter->ele)
			{
				t_ele_max = iter->ele;
			}
		}
		iter = iter->next;
		a++;
	}
	
	*all = a;
	*latmin = t_latmin;
	*latmax = t_latmax;
	*lonmin = t_lonmin;
	*lonmax = t_lonmax;
	*ele_min = t_ele_min;
	*ele_max = t_ele_max;
	
}

int plot(FILE *outputfile, track *trackToPlot, waypt *wayptToPlot, int width, int height)
{
	double wid, hei, stroke, sml, mul, elemul;
	double latmax, lonmax, latmin, lonmin, latd, lond, clat, clon, ele_max, ele_min, eled;
	unsigned int all = 0;
	char color1[] = "#333333\0";
	char color2[] = "#000000\0";
	char color3[] = "#FF0000\0";
	//char color4[] = "#DDDDDD\0";
	wid = width;
	hei = height;
	track *iter = NULL;
	waypt *iter2 = NULL;
	
	if (wid < hei)
	{
		stroke = sqrt(clamp(wid / 100, 1, 100));
		sml = wid - stroke * 20;
		stroke /= 2.0;
	}
	else
	{
		stroke = sqrt(clamp(hei / 100, 1, 100));
		sml = hei - stroke * 20;
		stroke /= 2.0;
	}
	
	if (trackToPlot != NULL)
	{
		iter = trackToPlot;
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 3;
	}
	
	iter2 = wayptToPlot;
	
	latlonminmax(trackToPlot, &latmin, &latmax, &lonmin, &lonmax, &ele_min, &ele_max, &all);
	
	latd = fabs(latmax - latmin);
	lond = fabs(lonmax - lonmin);
	eled = fabs(ele_max - ele_min);
	
	if ((sml / latd) < (sml / lond))
	{
		mul = sml / latd;
	}
	else
	{
		mul = sml / lond;
	}
	
	elemul = sml / eled;
	if (elemul)
		printf("Elevation plot is still work in progress\n");
	
	printf("Middle point: Lat: %f, Lon: %f\n", latmin + latd / 2, lonmin + lond / 2);
	
	// Begin SVG file
	fprintf(outputfile, "<html>\n<body>\n<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"");
	fprintf(outputfile, " width=\"%dpx\" height=\"%dpx\"", width, height);
	fprintf(outputfile, " preserveAspectRatio=\"none\">\n");
	fprintf(outputfile, "<g>\n");
	
	// Create axes
	fprintf(outputfile, "<rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\" fill=\"white\"/>", width, height);
	
	fprintf(outputfile, "<polyline points=\"%f,%f %f,%f\"", 0.0,hei/2, wid,hei/2);
	fprintf(outputfile, " fill=\"none\" stroke=\"%s\" stroke-width=\"%f\" />\n", color1, stroke);
	
	fprintf(outputfile, "<text x=\"%f\" y=\"%f\" stroke=\"none\" fill=\"%s\"", 0.0 + stroke * 5,hei/2 - stroke * 2, color1);
	fprintf(outputfile, " font-size=\"%f\">%f</text>\n", stroke * 12, lonmin + lond / 2);
	
	fprintf(outputfile, "<polyline points=\"%f,%f %f,%f\"", wid/2,0.0, wid/2,hei);
	fprintf(outputfile, " fill=\"none\" stroke=\"%s\" stroke-width=\"%f\" />\n", color1, stroke);
	
	fprintf(outputfile, "<text x=\"%f\" y=\"%f\" stroke=\"none\" fill=\"%s\"", wid/2 +  stroke * 5,0.0 + stroke * 10, color1);
	fprintf(outputfile, " font-size=\"%f\">%f</text>\n", stroke * 12, latmin + latd / 2);
	
	fprintf(outputfile, "\n");
	
	// Plot data using a polyline element
	
	/*
	fprintf(outputfile, "<polyline points=\"");
	clon = 0;
	while (iter != NULL)
	{
		clat = (iter->ele - ele_max);
		clon += 1;//sml / all;
		
		clat = clat * elemul + ((hei - sml) / 2) - 1000;
		//clon = clon * ((wid - sml) / 2);
		
		fprintf(outputfile, "%0.10f,%0.10f ", clon, clat);
		
		iter = iter->next;
	}
	fprintf(outputfile,  "\" fill=\"none\" stroke=\"%s\" stroke-width=\"%f\" />\n", color3, stroke);
	
	fprintf(outputfile, "\n");
	
	iter = trackToPlot;
	*/
	
	fprintf(outputfile, "<polyline points=\"");
	while (iter != NULL)
	{
		clat = fabs(iter->lat - latmax);
		clon = (iter->lon - lonmin);
		
		clat = clat * mul + ((hei - sml) / 2) + (clamp(lond - latd, 0, 1000) * mul) / 2;
		clon = clon * mul + ((wid - sml) / 2) + (clamp(latd - lond, 0, 1000) * mul) / 2;
		
		fprintf(outputfile, "%0.10f,%0.10f ", clon, clat);
		
		iter = iter->next;
	}
	fprintf(outputfile,  "\" fill=\"none\" stroke=\"%s\" stroke-width=\"%f\" />\n", color2, stroke);
	
	fprintf(outputfile, "\n");
	
	while (iter2 != NULL)
	{
		clat = fabs(iter2->lat - latmax);
		clon = (iter2->lon - lonmin);
		
		clat = clat * mul + ((hei - sml) / 2) + (clamp(lond - latd, 0, 1000) * mul) / 2;
		clon = clon * mul + ((wid - sml) / 2) + (clamp(latd - lond, 0, 1000) * mul) / 2;
		
		fprintf(outputfile, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" fill=\"%s\" />\n", clon, clat, stroke * 2, color3);
		fprintf(outputfile, "<text x=\"%f\" y=\"%f\" stroke=\"none\" fill=\"%s\"", clon, clat, color1);
		fprintf(outputfile, " font-size=\"%f\">%s</text>\n", stroke * 5, iter2->name);
		
		iter2 = iter2->next;
	}
	
	// Complete the HTML file
	fprintf(outputfile, "</g>\n</svg>\n</body>\n</html>\n");
	
	return 0;
}
