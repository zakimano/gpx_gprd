#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef calc_sh_on
#include "calc_sh.h"
#endif

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the haversine function
 */

int hav(double *lat1, double *lon1, double *lat2, double *lon2, double *distOut)
{
	double radLat1, radLat2, deltaLat, deltaLon, a, c, d;
	
	radLat1 = to_radians(*lat1);
	radLat2 = to_radians(*lat2);
	deltaLat = to_radians(*lat2 - *lat1);
	deltaLon = to_radians(*lon1 - *lon2);
	
	a = (sin(deltaLat / 2) * sin(deltaLat / 2) + cos(radLat1) * cos(radLat2) * (sin(deltaLon / 2) * sin(deltaLon / 2)));
	c = 2 * atan2(sqrt(a), sqrt(1 - a));
	d = RAD * c;
	
	*distOut = d;
	
	return 0;
}
