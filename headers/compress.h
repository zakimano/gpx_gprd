#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'compress'
 */

int compress(track *trackToCompress, double *angle)
{
	int i = 0;
	double xa, ya, brnga, xb, yb, brngb;
	double curAngle;
	double clat, clon, plat, plon, nlat, nlon;
	
	track *iter;
	if (trackToCompress->next->next != NULL)
	{
		iter = trackToCompress->next;
	}
	else
	{
		printf("\nERROR: Track too short, or track list not found.\n");
		return 3;
	}
	if (iter != NULL)
	{
		while (iter->next != NULL)
		{
			clat = to_radians(iter->lat);
			clon = to_radians(iter->lon);
			plat = to_radians(iter->prev->lat);
			plon = to_radians(iter->prev->lon);
			nlat = to_radians(iter->next->lat);
			nlon = to_radians(iter->next->lon);
			
			ya = sin(clon - plon) * cos(clat);
			xa = cos(plat) * sin(clat) - sin(plat) * cos(clat) * cos(clon - plon);
			brnga = to_degrees(atan2(ya, xa));
			
			yb = sin(nlon - clon) * cos(nlat);
			xb = cos(clat) * sin(nlat) - sin(clat) * cos(nlat) * cos(nlon - clon);
			brngb = to_degrees(atan2(yb, xb));
			
			curAngle = fabs(brngb - brnga);
			
			if (curAngle < *angle)
			{
				remove_track(iter->next);
				i++;
			}
			else
			{
				iter = iter->next;
			}
		}
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 3;
	}
	
	printf("Compressed: %d points removed.\n", i);
	
	return 0;
}
