#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define intersect_on 1

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef handle_coord_on
#include "handle_coord.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'intersect'
 */


int intersect(track* p0, track* p1, track* p2, track* p3, coord *interOut)
{
	/*
	Here's the vector math for doing it. I'm assuming the line from A to B is the line in question,
	and the line from C to D is one of the rectangle lines.
	My notation is that Ax is the "x-coordinate of A" and Cy is the "y-coordinate of C."
	And "*" means dot-product, so e.g. A*B = Ax*Bx + Ay*By.
	
	E = B-A = ( Bx-Ax, By-Ay )
	F = D-C = ( Dx-Cx, Dy-Cy ) 
	P = ( -Ey, Ex )
	h = ( (A-C) * P ) / ( F * P )
	
	This h number is the key. If h is between 0 and 1, the lines intersect, otherwise they don't.
	If F*P is zero, of course you cannot make the calculation,
	but in this case the lines are parallel and therefore only intersect in the obvious cases.
	
	The exact point of intersection is C + F*h.
	
	More Fun:
	
	If h is exactly 0 or 1 the lines touch at an end-point.
	You can consider this an "intersection" or not as you see fit.
	
	Specifically, h is how much you have to multiply the length of the line in order,
	to exactly touch the other line.
	
	Therefore, If h<0, it means the rectangle line is "behind" the given line
	(with "direction" being "from A to B"), and if h>1 the rectangle line is "in front" of the given line.
	*/
	
	coord A, B, C, D, E, F, P, E2, F2, P2, RET1, RET2;
	long double h = 0;
	
	/* ASSIGNING VALUES */
	
	A = c_make(*p0);
	B = c_make(*p1);
	C = c_make(*p2);
	D = c_make(*p3);
	
	/* CALCULATIONS */
	
	E = c_neg(B, A);		E2 = c_neg(D, C);
	F = c_neg(D, C);		F2 = c_neg(B, A);
	
	P.x = -E.y;				P2.x = -E2.y;
	P.y = E.x;				P2.y = E2.x;
	
	long double fpdot = c_dotp(F, P);
	long double fpdot2 = c_dotp(F2, P2);
	
	if (fpdot == 0)
	{
		if (c_same(A, C) || c_same(A, D))
			return 1;
		else
			return 0;
	}
	
	h = ( c_dotp( c_neg(A, C), P ) / fpdot );
	
	if (h < 1 && h > 0)
	{
		h = ( c_dotp( c_neg(C, A), P2 ) / fpdot2 );
		
		if (h < 1 && h> 0)
		{
			RET1.x = F2.x * h;
			RET1.y = F2.y * h;
			
			RET2 = c_add(A, RET1);
			
			*interOut = RET2;
			
			return 1;
		}
		
		return 0;
	}
	
	return 0;
}

int selfIntersect(track *toAnalyze, waypt **intersections, int *foundOut)
{
	track *iter = NULL;
	track *iter2 = NULL;
	waypt *first = NULL;
	waypt *last = NULL;
	waypt *last2 = NULL;
	coord inters;
	char fndc[100];
	int found = 0;
	
	if (toAnalyze != NULL)
	{
		iter = toAnalyze;
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 1;
	}
	
	while (iter->next->next->next != NULL)
	{
		iter2 = iter->next->next;
		while (iter2->next != NULL)
		{
			if (intersect(iter, iter->next, iter2, iter2->next, &inters))
			{
				found++;
				sprintf(fndc, "%d", found);
				if (found == 1)
				{
					first = create_waypt(inters.y, inters.x, 0, fndc);
					if (first != NULL)
					{
						last = first;
					}
					else
					{
						printf("\nERROR: Out of memory.\n");
						return 1;
					}
				}
				else
				{
					last2 = add_waypt(last, inters.y, inters.x, 0, fndc);
					if (last2 != NULL)
					{
						last = last2;
					}
					else
					{
						delete_waypt(first);
						printf("\nERROR: Out of memory.\n");
						return 1;
					}
				}
			}
			iter2 = iter2->next;
		}
		iter = iter->next;
	}
	
	*foundOut = found;
	
	*intersections = first;
	
	return 0;
}

int ABtrackIntersect(track *trackA, track *trackB, waypt **intersections, int *foundOut)
{
	track *iterA = NULL;
	track *iterB = NULL;
	waypt *first = NULL;
	waypt *last = NULL;
	waypt *last2 = NULL;
	coord inters;
	char fndc[100];
	int found = 0;
	
	if (trackA != NULL && trackB != NULL)
	{
		iterA = trackA;
		iterB = trackB;
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 1;
	}
	
	while (iterA->next != NULL)
	{
		iterB = trackB;
		while (iterB->next != NULL)
		{
			if (intersect(iterA, iterA->next, iterB, iterB->next, &inters))
			{
				found++;
				sprintf(fndc, "%d", found);
				if (found == 1)
				{
					first = create_waypt(inters.y, inters.x, 0, fndc);
					if (first != NULL)
					{
						last = first;
					}
					else
					{
						printf("\nERROR: Out of memory.\n");
						return 1;
					}
				}
				else
				{
					last2 = add_waypt(last, inters.y, inters.x, 0, fndc);
					if (last2 != NULL)
					{
						last = last2;
					}
					else
					{
						delete_waypt(first);
						printf("\nERROR: Out of memory.\n");
						return 1;
					}
				}
			}
			iterB = iterB->next;
		}
		iterA = iterA->next;
	}
	
	*foundOut = found;
	
	*intersections = first;
	
	return 0;
}
