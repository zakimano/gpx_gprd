#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'analyze'
 */

void p_analyze(double fullDist, double avgSpeed, double maxSpeed, double TdDist, double avgTDspeed, double totalUp, double totalDown, int fullTime, time_t beginTime, unsigned int pointCount)
{
	struct tm *info;
	char t_buffer[80];
	info = localtime(&beginTime);
	strftime(t_buffer, 80, "%Y %B %d %H:%M:%S", info);
	
	printf("Full length:    %0.3f km\t(%0.2f m)\n", fullDist / 1000, fullDist);
	printf("Full 3D length: %0.3f km\t(%0.2f m)\n", TdDist / 1000, TdDist);
	printf("Full time:      %2dh %2dm %2ds\n", fullTime / (60 * 60), (fullTime % (60 * 60)) / 60, fullTime % 60);
	printf("Average speed:  %0.2f km/h\t(%0.3f m/s)\n", avgSpeed * 3.6, avgSpeed);
	printf("Maximum speed:  %0.2f km/h\t(%0.3f m/s)\n", maxSpeed * 3.6, maxSpeed);
	printf("Total up:       %d m\nTotal down:     %d m\n", (int)totalUp, (int)totalDown);
	printf("Avg 3D speed:   %0.2f km/h\t(%0.3f m/s)\n", (avgTDspeed * 3.6), avgSpeed);
	printf("The track contains %d coordinates.\n", pointCount);
	printf("The track recorded started at: %s\n", t_buffer);
}

int analyze(track *trackToAnalyze, double *fullDistOut, double *TdDistOut, double *avgSpeedOut, double *maxSpeedOut, double *avgTDspeedOut, double *upOut, double *downOut, int *timeOut, time_t *beginTimeOut, unsigned int *pointCountOut)
{
	double curDist = 0, fullDist = 0, avgSpeed = 0, maxSpeed = 0, avgTDspeed = 0, curSpeed, TDDist = 0, curEle;
	unsigned int fullTime = 0, curTime = 0, pointCount = 0;
	
	track *iter;
	iter = trackToAnalyze;
	if (iter != NULL)
	{
		*beginTimeOut = iter->timev;
		while (iter->next != NULL)
		{
			hav(&iter->lat, &iter->lon, &iter->next->lat, &iter->next->lon, &curDist);
			curTime = difftime(iter->next->timev, iter->timev);
			curSpeed = curDist / curTime;
			if (curSpeed > maxSpeed)
			{
				maxSpeed = curSpeed;
			}
			
			curEle = iter->ele - iter->next->ele;
			
			(curEle < 0) ? (*downOut += curEle) : (*upOut += curEle);
			
			TDDist += sqrt(pow(curDist, 2) + pow(curEle, 2));
			
			fullDist += curDist;
			fullTime += curTime;
			iter = iter->next;
			pointCount++;
		}
		avgTDspeed = TDDist / fullTime;
		avgSpeed = fullDist / fullTime;
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 3;
	}
	
	*pointCountOut = pointCount;
	*timeOut = fullTime;	
	*avgTDspeedOut = avgTDspeed;
	*TdDistOut = TDDist;
	*fullDistOut = fullDist;
	*avgSpeedOut = avgSpeed;
	*maxSpeedOut = maxSpeed;
	
	return 0;
}
