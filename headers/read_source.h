#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'readSource'
 */

int readSource(FILE *fIn, char **strOut, unsigned int limit)
{	
	/* OWN ALLOCATED MEM TO READ INTO */
	
	unsigned int i;
	i = 0;
	
	char *strInA;
	strInA = (char *)calloc(limit, sizeof(char));
	
	char buffer = '0';
	
	while (buffer != 0)
	{
		buffer = fgetc(fIn); // expect 1 successful conversion
		if (feof(fIn))
		{
			break;
		}
		// process buffer
		
		strInA[i] = buffer;
		strInA[i + 1] = '\0';
		i++;
		
		if ((i + HBUF) >= limit)
		{
			limit += HINC;
			
			char *strInB = (char *)realloc(strInA, limit * sizeof(char));
			
			if (strInB != NULL)
			{
				strInA = strInB;
			}
			else
			{
				printf("\nERROR: Out of memory\n");
				free(strInA);
				return 1;
			}
		}
	}
	if (feof(fIn))
	{
		// hit end of file
		
		/* RETURN VALUES HERE */
		
		*strOut = strInA;
				
		return 0;
	}
	else
	{
		// some other error interrupted the read
		printf("\nERROR: Read interrupted\n");
		free(strInA);
		return 3;
	}
	
	free(strInA);
	
	return 3;
}

