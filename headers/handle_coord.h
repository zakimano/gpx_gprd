#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define handle_coord_on 1

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the handling of coordinates.
 */



typedef struct s_coord
{
	long double x;
	long double y;
} coord;

coord c_neg(coord a, coord b) //returns a-b
{
	coord retval;
	retval.x = a.x - b.x;
	retval.y = a.y - b.y;
	return retval;
}

coord c_add(coord a, coord b) //returns a+b
{
	coord retval;
	retval.x = a.x + b.x;
	retval.y = a.y + b.y;
	return retval;
}

long double c_dotp(coord a, coord b) //dot-product
{
	long double retval;
	retval = (a.x * b.x) + (a.y * b.y);
	return retval;
}

int c_same(coord a, coord b) //checks if the coords are the same.
{
	return (a.x == b.x && a.y == b.y) ? 1 : 0;
}

coord c_make(track a) //converting to coord
{
	coord retval;
	retval.x = a.lon;
	retval.y = a.lat;
	return retval;
}
