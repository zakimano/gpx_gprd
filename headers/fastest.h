#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'fastest'
 */

void p_fastest(double fastSpeed, double fastDist, double fastBegin, int fastTime, double arg_dist)
{
	printf("The fastest\t%0.1f m:\n", arg_dist);
	printf("Starts from %.3f km, ends at: %.3f km\n", fastBegin / 1000, (fastBegin + fastDist) / 1000);
	printf("Actual length:\t%0.2f m\n", fastDist);
	printf("Time:\t\t%2d h %2d m %2d s\n", fastTime / (60 * 60), (fastTime % (60 * 60)) / 60, fastTime % 60);
	printf("Average speed:\t%0.2f km/h (%0.3f m/s)\n", (fastSpeed * 3.6), fastSpeed);
}

int fastest(track *trackToAnalyze, double *beginOut, double *speedOut, double *distOut, int *timeOut, double *lengthIn)
{
	double curDist = 0, longCurDist = 0, longCurSpeed = 0, fastLongSpeed = 0, fastLongDist = 0, fullLen = 0, fastBegin;
	unsigned int curTime = 0, longCurTime = 0, fastLongTime = 0;
	
	track *iter = NULL, *Siter = NULL;
	
	iter = Siter = trackToAnalyze;
	if (iter != NULL)
	{
		while (Siter->next != NULL)
		{
			longCurDist = 0;
			longCurTime = 0;
			
			iter = Siter;
			while (*lengthIn > longCurDist && iter->next != NULL)
			{
				hav(&iter->lat, &iter->lon, &iter->next->lat, &iter->next->lon, &curDist);
				curTime = difftime(iter->next->timev, iter->timev);
				
				longCurDist += curDist;
				longCurTime += curTime;
				fullLen += curDist;
				
				iter = iter->next;
				
			}
			
			longCurSpeed = longCurDist / longCurTime;
			
			if (longCurSpeed > fastLongSpeed)
			{
				fastLongTime = longCurTime;
				fastLongSpeed = longCurSpeed;
				fastLongDist = longCurDist;
				fastBegin = fullLen - longCurDist;
			}
			
			Siter = Siter->next;
		}
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 3;
	}
	
	*timeOut = fastLongTime;
	*beginOut = fastBegin;
	*speedOut = fastLongSpeed;
	*distOut = fastLongDist;
	
	return 0;
}
