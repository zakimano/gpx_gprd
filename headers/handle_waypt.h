#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define handle_wpt_on 1

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for waypoint handling
 */

typedef struct s_waypt
{
	struct s_waypt* prev;
	char name[HINC];
	double lat;
	double lon;
	double ele;
	struct s_waypt* next;
} waypt;

waypt* create_waypt(double nlat, double nlon, double nele, char *nname)
{
	waypt* newWaypt = malloc(sizeof(waypt));
	if (newWaypt != NULL)
	{
		newWaypt->prev = NULL;
		newWaypt->lat = nlat;
		newWaypt->lon = nlon;
		newWaypt->ele = nele;
		memcpy(newWaypt->name, nname, (sizeof(char) * HINC));
		newWaypt->next = NULL;
	}
	return newWaypt;
}

void delete_waypt(waypt *oldWaypt)
{
	if (oldWaypt->next != NULL)
	{
		delete_waypt(oldWaypt->next);
	}
	free(oldWaypt);
}

waypt* add_waypt(waypt *wayptListLast, double nlat, double nlon, double nele, char *nname)
{
	waypt* newWaypt = create_waypt(nlat, nlon, nele, nname);
	if (newWaypt != NULL)
	{
		wayptListLast->next = newWaypt;
		newWaypt->prev = wayptListLast;
	}
	return newWaypt;
}

void remove_waypt(waypt *toRemove)
{
	waypt *prevWaypt;
	waypt *nextWaypt;
	
	prevWaypt = toRemove->prev;
	nextWaypt = toRemove->next;
	
	if (prevWaypt != NULL)
		prevWaypt->next = nextWaypt;
	if (nextWaypt != NULL)
		nextWaypt->prev = prevWaypt;
	
	free(toRemove);
}
