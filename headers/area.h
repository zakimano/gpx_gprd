#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef intersect_on
#include "intersect.h"
#endif

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef handle_coord_on
#include "handle_coord.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

#ifndef SEH
#define SEH(err) ({if (err != 0) {return err;}})
#endif

/*
 * Header file for the function 'trackArea'
 */

double trackArea(track *trackToArea, double *areaOut)
{
	/*
	function polygonArea(X, Y, numPoints) 
	{
		area = 0;         // Accumulates area in the loop
		j = numPoints-1;  // The last vertex is the 'previous' one to the first
		
		for (i=0; i<numPoints; i++)
		{ area = area +  (X[j]+X[i]) * (Y[j]-Y[i]); 
			j = i;  //j is previous vertex to i
		}
		return area/2;
	}
	*/
	
	
	track *iter;
	track *iter2;
	double x1, y1, x2, y2;
	x1 = y1 = x2 = y2 = 0;
	
	if (trackToArea != NULL)
	{
		iter = trackToArea;
		for (iter2 = iter; iter2->next != NULL; iter2 = iter2->next);
	}
	else
	{
		printf("\nERROR: Track list not found\n");
		return 1;
	}
	
	double area = 0, mul, mlon, mlat, tval;
	tval = iter->lon + 1.0;
	hav(&iter->lon, &iter->lon, &iter->lon, &tval, &mlon);
	tval = iter2->lat + 1.0;
	hav(&iter2->lat, &iter2->lat, &iter2->lat, &tval, &mlat);
	mul = (mlat + mlon) / 2.0;
	mul = pow(mul, 2);
	
	for (; iter->next != NULL; iter = iter->next)
	{
		x1 = iter->lon;
		y1 = iter->lat;
		x2 = iter2->lon;
		y2 = iter2->lat;
		
		area += ((x2 + x1) * (y2 - y1));
		
		//area += ((x1 * y2) - (y1 * x2));
		
		iter2 = iter;
	}
	area /= 2.0;
	area = fabs(area * mul);
	
	*areaOut = area;
	
	return 0;
}

int area(track *trackToAnalyze)
{
	int err = 0;
	double area = 0;
	waypt *inters = NULL;
	int inters_found = 0;
	
	err = trackArea(trackToAnalyze, &area);
	SEH(err);
	
	err = selfIntersect(trackToAnalyze, &inters, &inters_found);
	SEH(err);
	
	if (inters_found)
	{
		printf("Warning: Track intersecting with itself at %d points. Area data may be incorrect.\n", inters_found);
	}
	printf("Full area of the track: %0.3f square km (%0.2f sqare m)\n", area / 1000000, area);
	
	if (inters != NULL)
	{
		delete_waypt(inters);
	}
	
	return 0;
}