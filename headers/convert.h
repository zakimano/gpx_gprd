#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'convert'
 */

int convert(char *str_latlon, char *str_ele, char *str_time, long double *c_lat, long double *c_lon, long double *c_ele, time_t *c_time)
{
	int i = 0;
	
	double convertedLat, convertedLon, convertedEle;
	time_t convertedTime = 0;
	char *tok;
	char *ret;
	char convs[6][80];
	
	/* lat="46.57608333" lon="8.89241667" */
	tok = strtok_r(str_latlon, "\"", &ret);
	while (tok != NULL)
	{
		memcpy(convs[i], tok, (strlen(tok) + 1) * sizeof(char));
		tok = strtok_r(NULL, "\"", &ret);
		i++;
	}
	convertedLat = atof(convs[1]);
	convertedLon = atof(convs[3]);
	
	/* 2376 */
	convertedEle = atof(str_ele);
	
	/* 2007-10-14T10:09:57Z */
	i = 0;
	const char delimit[] = "-:TtZz";
	ret = NULL;
	tok = strtok_r(str_time, delimit, &ret);
	while (tok != NULL)
	{
		memcpy(convs[i], tok, (strlen(tok) + 1) * sizeof(char));
		tok = strtok_r(NULL, delimit, &ret);
		i++;
	}
	
	struct tm tconv;
	tconv.tm_year = atoi(convs[0]) - 1900;
	tconv.tm_mon = atoi(convs[1]);
	tconv.tm_mday = atoi(convs[2]);
	tconv.tm_hour = atoi(convs[3]);
	tconv.tm_min = atoi(convs[4]);
	tconv.tm_sec = atoi(convs[5]);
	
	i = 0;
	
	convertedTime = mktime(&tconv);
	
	/* PASSING STUFF */
	
	*c_lat = convertedLat;
	*c_lon = convertedLon;
	*c_ele = convertedEle;
	*c_time = convertedTime;
	
	return 0;
}
