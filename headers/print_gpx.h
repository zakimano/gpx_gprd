#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'printOutputGPX'
 */

int printOutputGPX(FILE *outputfile, track *trackToPrint, waypt *wayptsToPrint)
{
	char buffer[21];
	struct tm *tm_info;
	
	track *iter;
	waypt *iter2;
	
	iter = trackToPrint;
	iter2 = wayptsToPrint;
	
	fprintf(outputfile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fprintf(outputfile, "<gpx version=\"1.0\">\n");
	while (iter2 != NULL)
	{
		fprintf(outputfile, "\t<wpt lat=\"%f\" lon=\"%f\">\n", iter2->lat, iter2->lon);
		fprintf(outputfile, "\t\t<ele>%f</ele>\n", iter2->ele);
		fprintf(outputfile, "\t\t<name>%s</name>\n", iter2->name);
		fprintf(outputfile, "\t</wpt>\n");
		
		iter2 = iter2->next;
	}
	fprintf(outputfile, "\t<trk>\n");
	fprintf(outputfile, "\t\t<trkseg>\n");
	while (iter != NULL)
	{
		tm_info = localtime(&iter->timev);
		//2007-10-14T10:09:57Z
		strftime(buffer, 21, "%Y-%m-%dT%H:%M:%SZ", tm_info);
		
		fprintf(outputfile, "\t\t\t<trkpt lat=\"%f\" lon=\"%f\">\n", iter->lat, iter->lon);
		fprintf(outputfile, "\t\t\t\t<ele>%f</ele>\n", iter->ele);
		fprintf(outputfile, "\t\t\t\t<time>%s</time>\n", buffer);
		fprintf(outputfile, "\t\t\t</trkpt>\n");
		
		iter = iter->next;
	}
	fprintf(outputfile, "\t\t</trkseg>\n");
	fprintf(outputfile, "\t</trk>\n");
	fprintf(outputfile, "</gpx>");
	
	return 0;
}
