#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define calc_sh_on 1

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for small calculations
 */

double to_degrees(double rad)
{
	return rad * (180.0 / M_PI);
}

double to_radians(double deg)
{
	return deg * (M_PI / 180.0);
}

double clamp(double in, double min, double max)
{
	if (in > max)
	{
		return max;
	}
	else if (in < min)
	{
		return min;
	}
	else
	{
		return in;
	}
}

int sgn(double val)
{
	return val < 0 ? -1 : 1;
}