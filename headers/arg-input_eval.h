#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

#define PBUFF 4000 //used for print buffer, to print text from source files

/*
 * Header file for input and argument evaluation
 */

int p_error(int retval, char *evals, char type[], char func[], char arg[])
{
	char buffer[PBUFF];
	char *index;
	int argsize, l;
	printf("\nERROR: %s\n", type);
	if (strstr(func, "usage"))
	{
		index = strstr(evals, func);
		argsize = strlen(func) + 1;
		for (l = 0; *(index + argsize + l) != '#'; l++);
		memcpy(buffer, index + argsize, l);
		buffer[l] = '\0';
		printf("Usage: %s%s\n", arg, buffer);
	}
	return retval;
}

void p_help(char *evals, char funch[], char func[], char arg[])
{
	char buffer[PBUFF];
	char *index;
	int argsize, l;
	printf("Vewing help");
	if (strstr(funch, "help"))
	{
		index = strstr(evals, funch);
		argsize = strlen(funch) + 1;
		for (l = 0; *(index + argsize + l) != '#'; l++);
		memcpy(buffer, index + argsize, l);
		buffer[l] = '\0';
		printf(" %s\n", buffer);
	}
	
	if (strstr(func, "usage"))
	{
		index = strstr(evals, func);
		argsize = strlen(func) + 1;
		for (l = 0; *(index + argsize + l) != '#'; l++);
		memcpy(buffer, index + argsize, l);
		buffer[l] = '\0';
		printf("Usage: %s%s\n", arg, buffer);
	}
	
}

int argeval(char *margv)
{
	if (strcmp(margv, "analyze") == 0)
	{
		return 1;
	}
	else if (strcmp(margv, "compress") == 0)
	{
		return 2;
	}
	else if (strcmp(margv, "plot") == 0)
	{
		return 3;
	}
	else if (strcmp(margv, "help") == 0)
	{
		return 4;
	}
	else if (strcmp(margv, "correct") == 0)
	{
		return 5;
	}
	else if (strcmp(margv, "fastest") == 0)
	{
		return 6;
	}
	else if (strcmp(margv, "area") == 0)
	{
		return 7;
	}
	else if (strcmp(margv, "intersect") == 0)
	{
		return 8;
	}
	else
	{
		return 0;
	}
}

int inputeval(int margc, char **margv, FILE **inputfile, FILE **inputfile2, FILE **outputfile, char **evalsOut)
{
	FILE *in = NULL;
	FILE *in2 = NULL;
	FILE *out = NULL;
	FILE *evaltext = NULL;
	char *evals = NULL;
	evaltext = fopen("sources/eval.text", "r");
	if (evaltext == NULL)
	{
		return p_error(3, evals, "Can't find source file \"eval.text\"", "-", "-");
	}
	else
	{
		readSource(evaltext, &evals, HINC);
	}
	fclose(evaltext);
	*evalsOut = evals;
	
	if (margc == 1)
	{
		return p_error(2, evals, "Bad input", "usage-main", margv[0]);
	}
	switch (argeval(margv[1]))
	{
		case 1: /* analyze */
			if (margc == 3)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-analyze", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-analyze", margv[0]);
			}
			break;
		case 2: /* compress */
			if (margc == 5)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-compress", margv[0]);
				}
				if (atof(margv[3]) < 0 || atof(margv[3]) > 180)
				{
					return p_error(2, evals, "Bad input", "usage-compress-angle", margv[0]);
				}
				if (strstr(margv[4], ".gpx"))
				{
					out = fopen(margv[4], "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-compress", margv[0]);
					}
				}
				else
				{
					// APPEND .gpx
					char toOpen[HINC];
					toOpen[0] = '\0';
					strcat(toOpen, margv[4]);
					strcat(toOpen, ".gpx\0");
					
					out = fopen(toOpen, "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-compress", margv[0]);
					}
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-compress", margv[0]);
			}
			break;
		case 3: /* plot */
			if (margc == 6)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-plot", margv[0]);
				}
				if (strstr(margv[3], ".svg"))
				{
					out = fopen(margv[3], "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-plot", margv[0]);
					}
				}
				else
				{
					// APPEND .svg
					char toOpen[HINC];
					toOpen[0] = '\0';
					strcat(toOpen, margv[3]);
					strcat(toOpen, ".svg\0");
					
					out = fopen(toOpen, "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-plot", margv[0]);
					}
				}
				if (atoi(margv[4]) < 100)
				{
					return p_error(2, evals, "Bad input", "usage-plot-size", margv[0]);
				}
				if (atoi(margv[5]) < 100)
				{
					return p_error(2, evals, "Bad input", "usage-plot-size", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-plot", margv[0]);
			}
			break;
		case 4:
			if (margc == 3)
			{
				if (argeval(margv[2]) == 0) //error
				{
					return p_error(2, evals, "Bad input", "usage-help", margv[0]);
				}
				if (argeval(margv[2]) == 1) //analyze
				{
					p_help(evals, "help-analyze", "usage-analyze", margv[0]);
				}
				if (argeval(margv[2]) == 2) //compress
				{
					p_help(evals, "help-compress", "usage-compress", margv[0]);
				}
				if (argeval(margv[2]) == 3) //plot
				{
					p_help(evals, "help-plot", "usage-plot", margv[0]);
				}
				if (argeval(margv[2]) == 5) //correct
				{
					p_help(evals, "help-correct", "usage-correct", margv[0]);
				}
				if (argeval(margv[2]) == 6) //fastest
				{
					p_help(evals, "help-fastest", "usage-fastest", margv[0]);
				}
				if (argeval(margv[2]) == 7) //area
				{
					p_help(evals, "help-area", "usage-area", margv[0]);
				}
				if (argeval(margv[2]) == 8) //intersect
				{
					p_help(evals, "help-intersect", "usage-intersect", margv[0]);
				}
			}
			else if (margc == 2)
			{
				p_help(evals, "help-general", "-", "-");
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-help", margv[0]);
			}
			return 4;
			break;
		case 5: /* correct */
			if (margc == 5)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-correct", margv[0]);
				}
				if (strstr(margv[3], ".gpx"))
				{
					out = fopen(margv[3], "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-correct", margv[0]);
					}
				}
				else
				{
					// APPEND .gpx
					char toOpen[HINC];
					toOpen[0] = '\0';
					strcat(toOpen, margv[3]);
					strcat(toOpen, ".gpx\0");
					
					out = fopen(toOpen, "w+");
					if (out == NULL)
					{
						return p_error(2, evals, "Bad input, or could not create file", "usage-correct", margv[0]);
					}
				}
				if (atof(margv[4]) < 0)
				{
					return p_error(2, evals, "Bad input", "usage-correct-speed", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-correct", margv[0]);
			}
			break;
		case 6: /* fastest */
			if (margc == 4)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-fastest", margv[0]);
				}
				if (atof(margv[3]) < 0)
				{
					return p_error(2, evals, "Bad input", "usage-fastest-dist", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-fastest", margv[0]);
			}
			break;
		case 7: /* area */
			if (margc == 3)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-area", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-area", margv[0]);
			}
			break;
		case 8: /* intersect */
			if (margc == 4)
			{
				in = fopen(margv[2], "r");
				if (in == NULL)
				{
					return p_error(2, evals, "Bad input, or could not find file", "usage-intersect", margv[0]);
				}
				if (strcmp(margv[2], margv[3]) != 0)
				{
					in2 = fopen(margv[3], "r");
					if (in2 == NULL)
					{
						return p_error(2, evals, "Bad input, or could not find file", "usage-intersect", margv[0]);
					}
				}
				
				char toOpen[HINC];
				memcpy(toOpen, "output.gpx\0", sizeof(char) * 11);
				/*
				toOpen[0] = '\0';
				strcat(toOpen, margv[2]);
				strcat(toOpen, " - ");
				strcat(toOpen, margv[3]);
				strcat(toOpen, ".gpx\0");
				*/
				
				out = fopen(toOpen, "w+");
				if (out == NULL)
				{
					return p_error(2, evals, "Bad input, or could not create file", "usage-intersect", margv[0]);
				}
			}
			else
			{
				return p_error(2, evals, "Bad input", "usage-intersect", margv[0]);
			}
			break;
		default:
			return p_error(2, evals, "Bad input", "usage-main", margv[0]);
			break;
	}
	
	*inputfile = in;
	*inputfile2 = in2;
	*outputfile = out;
	
	return 0;
}
