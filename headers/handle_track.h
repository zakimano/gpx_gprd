#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#define handle_track_on 1

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for track handling
 */

typedef struct s_track
{
	struct s_track* prev;
	double lat;
	double lon;
	double ele;
	time_t timev;
	struct s_track* next;
} track;

track* create_track(double nlat, double nlon, double nele, time_t ntimev)
{
	track* newTrack = malloc(sizeof(track));
	if (newTrack != NULL)
	{
		newTrack->prev = NULL;
		newTrack->lat = nlat;
		newTrack->lon = nlon;
		newTrack->ele = nele;
		newTrack->timev = ntimev;
		newTrack->next = NULL;
	}
	return newTrack;
}

void delete_track(track *oldTrack)
{
	if (oldTrack->next != NULL)
	{
		delete_track(oldTrack->next);
	}
	free(oldTrack);
}

track* add_track(track *trackListLast, double nlat, double nlon, double nele, time_t ntimev)
{
	track* newTrack = create_track(nlat, nlon, nele, ntimev);
	if (newTrack != NULL)
	{
		trackListLast->next = newTrack;
		newTrack->prev = trackListLast;
	}
	return newTrack;
}

void remove_track(track *toRemove)
{
	track *prevTrack;
	track *nextTrack;
	
	prevTrack = toRemove->prev;
	nextTrack = toRemove->next;
	
	if (prevTrack != NULL)
		prevTrack->next = nextTrack;
	
	if (nextTrack != NULL)
		nextTrack->prev = prevTrack;
	
	free(toRemove);
}
