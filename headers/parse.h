#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'parse'
 */


int parse(char *str_input, track **trackOut, waypt **wayptOut)
{
	int err = 0;
	int trk = 0;
	int lasttrk = 1;
	int wpts = 0;
	int lastwpts = 0;
	
	char *tokenize;
	char* tok;
	char delimit[] = "<>";
	tok = strtok_r(str_input, delimit, &tokenize);
	
	char* tmp_latlon;
	tmp_latlon = (char *)calloc(HINC, sizeof(char));
	char* tmp_ele;
	tmp_ele = (char *)calloc(HINC, sizeof(char));
	char* tmp_time;
	tmp_time = (char *)calloc(HINC, sizeof(char));
	char* tmp_name;
	tmp_name = (char *)calloc(HINC, sizeof(char));
	
	long double c_lat;
	long double c_lon;
	long double c_ele;
	time_t c_time;
	
	track* firstTrack = NULL;
	track* lastTrack = NULL;
	
	waypt* firstWaypt = NULL;
	waypt* lastWaypt = NULL;
	
	
	if (tok == NULL || tmp_latlon == NULL || tmp_ele == NULL || tmp_time == NULL || tmp_name == NULL)
	{
		printf("\nERROR: Out of memory\n");
		return 1;
	}
	
	while (tok != NULL)
	{
		if (strstr(tok, "wpt "))
		{
			while (strcmp(tok, "/wpt") != 0)
			{
				/*
				if (strcmp(tok, "/rtept") == 0)
				{
					break;
				}
				*/
				if (strstr(tok, "wpt"))
				{
					memcpy(tmp_latlon, tok, (strlen(tok) * sizeof(char)));
				}
				if (strcmp(tok, "ele") == 0)
				{
					tok = strtok_r(NULL, delimit, &tokenize);
					memcpy(tmp_ele, tok, (strlen(tok) * sizeof(char)));
				}
				if (strcmp(tok, "name") == 0)
				{
					tok = strtok_r(NULL, delimit, &tokenize);
					memcpy(tmp_name, tok, (strlen(tok) * sizeof(char)));
				}
				tok = strtok_r(NULL, delimit, &tokenize);
			}
			wpts++;
		}
		
		if (strcmp(tok, "trk") == 0 || strcmp(tok, "rte") == 0)
		{
			trk = 1;
		}
		if (strcmp(tok, "/trk") == 0 || strcmp(tok, "/rte") == 0)
		{
			trk = 0;
		}
		if (strstr(tok, "trkpt ") || strstr(tok, "rtept "))
		{
			while (strcmp(tok, "/trkpt") != 0)
			{
				if (strcmp(tok, "/rtept") == 0)
				{
					break;
				}
				if (strstr(tok, "trkpt") || strstr(tok, "rtept"))
				{
					memcpy(tmp_latlon, tok, (strlen(tok) * sizeof(char)));
				}
				if (strcmp(tok, "ele") == 0)
				{
					tok = strtok_r(NULL, delimit, &tokenize);
					memcpy(tmp_ele, tok, (strlen(tok) * sizeof(char)));
				}
				if (strcmp(tok, "time") == 0)
				{
					tok = strtok_r(NULL, delimit, &tokenize);
					memcpy(tmp_time, tok, (strlen(tok) * sizeof(char)));
				}
				tok = strtok_r(NULL, delimit, &tokenize);
			}
			trk++;
		}
		
		if (trk > lasttrk)
		{
			if (trk == 2)
			{
				err = convert(tmp_latlon, tmp_ele, tmp_time, &c_lat, &c_lon, &c_ele, &c_time);
				if (err != 0)
				{
					return err;
				}
				firstTrack = create_track(c_lat, c_lon, c_ele, c_time);
				if (firstTrack == NULL)
				{
					printf("\nERROR: Out of memory\n");
					return 1;
				}
				lastTrack = firstTrack;
			}
			else
			{
				err = convert(tmp_latlon, tmp_ele, tmp_time, &c_lat, &c_lon, &c_ele, &c_time);
				if (err != 0)
				{
					return err;
				}
				lastTrack = add_track(lastTrack, c_lat, c_lon, c_ele, c_time);
				if (lastTrack == NULL)
				{
					printf("\nERROR: Out of memory\n");
					return 1;
				}
			}
			lasttrk = trk;
		}
		else if (wpts > lastwpts)
		{
			if (wpts == 1)
			{
				err = convert(tmp_latlon, tmp_ele, tmp_time, &c_lat, &c_lon, &c_ele, &c_time);
				if (err != 0)
				{
					return err;
				}
				firstWaypt = create_waypt(c_lat, c_lon, c_ele, tmp_name);
				if (firstWaypt == NULL)
				{
					printf("\nERROR: Out of memory\n");
					return 1;
				}
				lastWaypt = firstWaypt;
			}
			else
			{
				err = convert(tmp_latlon, tmp_ele, tmp_time, &c_lat, &c_lon, &c_ele, &c_time);
				if (err != 0)
				{
					return err;
				}
				lastWaypt = add_waypt(lastWaypt, c_lat, c_lon, c_ele, tmp_name);
				if (lastWaypt == NULL)
				{
					printf("\nERROR: Out of memory\n");
					return 1;
				}
			}
			lastwpts = wpts;
		}
		
		tok = strtok_r(NULL, delimit, &tokenize);
		
	}
	
	/* trackOut should store the first member of the list */
	
	*trackOut = firstTrack;
	*wayptOut = firstWaypt;
	
	/* store metadata too? */
	
	free(tok);
	free(tmp_latlon);
	free(tmp_ele);
	free(tmp_time);
	free(tmp_name);
	
	
	return 0;
}
