#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#ifndef handle_track_on
#include "handle_track.h"
#endif

#ifndef handle_wpt_on
#include "handle_waypt.h"
#endif

#ifndef RAD
#define RAD 6371000
#endif

#ifndef HINC
#define HINC 1000
#endif

#ifndef HBUF
#define HBUF 10
#endif

/*
 * Header file for the function 'correct'
 */

int correct(track *trackToCorrect, double *speed)
{
	int i = 0, err = 0;
	double curSpeed, curTime, curDist;
	double clat, clon, nlat, nlon;
	
	track *iter;
	if (trackToCorrect != NULL)
	{
		iter = trackToCorrect;
	}
	else
	{
		printf("\nERROR: Track too short, or track list not found.\n");
		return 3;
	}
	
	if (iter != NULL)
	{
		while (iter->next != NULL)
		{
			clat = iter->lat;
			clon = iter->lon;
			nlat = iter->next->lat;
			nlon = iter->next->lon;
			
			err = hav(&clat, &clon, &nlat, &nlon, &curDist);
			if (err != 0)
			{
				printf("\nERROR: \n");
				return 1;
			}
			
			curTime = difftime(iter->next->timev, iter->timev);
			
			curSpeed = curDist / curTime;
			
			if (curSpeed > *speed)
			{
				remove_track(iter->next);
				i++;
			}
			else
			{
				iter = iter->next;
			}
		}
	}
	else
	{
		printf("\nERROR: Track list not found.\n");
		return 3;
	}
	
	printf("Corrected: %d points removed.\n", i);
	
	return 0;
}
