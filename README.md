gpx gprd
========

GPX Gepard
----------

This piece of (pretty horrible) work is an attempt at creating something to analyze GPX files with.
It should be able to tell you some interesting data about the GPX file you throw at it.

No guarantees that it'll work.

In the slight chance that someone finds this useful, and tells me about it, I might get back to it, to clean it up, fasten, and overall improve it...
Otherwise it'll probably sit here untouched for a while.